# README

Maven tiles is a set of tiles which you can use to build maven projects.

Look also at: http://kkapuscik.bitbucket.org/

***

## What is this repository for?

The repository holds source code of the "tiles" for building maven projects.

***

## Version

The tile set is dynamic, so there is no version information. Each tile is versioned separately according to maven rules.

***

## How do I get set up?

Following information describes what is needed to build the project.

### Set up needed

The project requires:
* Maven (hey, these are maven tiles...)

### Summary of set up

To prepare the project:
* Use the Maven Tiles plugin: https://github.com/repaint-io/maven-tiles
* Use the tiles you want for your project.

### How to run tests

Not sure if there will be tests... If any then this will use maven tests goal.

### Deployment instructions

Deployment is not yet defined.

***

## Contribution guidelines

Following contribution guidelines are defined:

* No contribution guidelines are currently set. Just try to follow the XML formatting.

***

## Who do I talk to?

### Repo owner or admin

Krzysztof Kapuscik (kkapuscik) - k.kapuscik@gmail.com

***

## Useful links

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)